<?php


class Localizacao
{

    public function __construct(
        private float $longitude,
        private float $latitude
    ){

    }

    function getLatitude() {
        return $this->latitude;
    }

    function getLongitude(){
        return $this->longitude;
    }

    function distanceTo(Localizacao $b) {
        //R * arccos (sin (lata) * sin (latB) + cos (lata) * cos (latB) * cos (Lona-lonB)) 
        return 6372.795477598 * acos( sin( $this->latitude) * sin($b->latitude) + cos($this->latitude) * cos($b->latitude) * cos($this->longitude - $b->longitude));
    }
}

interface GetLocalizcao {
    function getLocalizcao();
}

trait LocalizacaoTrait
{
    public function getLocalizcao()
    {
        return $this->localizacao;
    }

}


class Restaurante implements GetLocalizcao
{
    use LocalizacaoTrait;

    public function __construct(
        private Localizacao $localizacao
    ) {

    }
}


class Cliente implements GetLocalizcao
{
    use LocalizacaoTrait;

    public function __construct(
        private Localizacao $localizacao
    ) {

    }
}

class Entregador implements GetLocalizcao
{
    use LocalizacaoTrait;

    public function __construct(
        private Localizacao $localizacao
    ) {

    }
}

interface CalculadoraDeTaxa {

    /**
     * 
     */
    function calcular(array $localizacoes);
}


class TaxaPorDistancia implements CalculadoraDeTaxa{

    function __construct(private float $precoPorKm)
    {
        
    }

    function calcular(array $localizacoes) {
        $distancia = 0;
        $a = $localizacoes[0]->getLocalizcao();
        foreach($localizacoes as $localizacao) {
            $distancia += $localizacao->getLocalizcao()->distanceTo($a);
            $a = $localizacao->getLocalizcao();
        }
        return $distancia * $this->precoPorKm;

    }

}

class TaxaFixa implements CalculadoraDeTaxa {

    public function __construct(private float $taxa)
    {
        
    }

    public function calcular(array $localizacoes) {
        return $this->taxa;
    }
}

$localizacoes = [
    new Entregador(new Localizacao(-22.234132, -49.970119)),
    new Restaurante(new Localizacao(-22.231978, -49.967925)),
    new Cliente(new Localizacao(-22.232650, -49.968650)),
];

//$calculadora = new TaxaFixa(5);
$calculadora = new TaxaPorDistancia(1);
var_dump($calculadora->calcular($localizacoes));
