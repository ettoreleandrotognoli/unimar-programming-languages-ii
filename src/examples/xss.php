<?php


class Vulnerability
{
    function __construct(
        private $offset,
        private $length,
        private $content,
        private $source
    ) {
    }
}

interface VulnerabilitySearcher
{

    function search(string $input): array;
}

class ComposedVulnerabilitySearcher implements VulnerabilitySearcher
{

    function __construct(
        private array $searchers
    ) {
    }

    function search(string $input): array
    {
        $result = [];
        foreach ($this->searchers as $searcher) {
            $result = array_merge($result, $searcher->search($input));
        }
        return $result;
    }
}

class KeyWordsVulnerabilitySearcher implements VulnerabilitySearcher
{

    function __construct(
        private array $keyWords
    ) {
    }

    function search(string $input): array
    {
        $result = [];
        foreach ($this->keyWords as $keyword) {
            $index = strpos($input, $keyword);
            if ($index === false) {
                continue;
            }
            $result[] = new Vulnerability($index, strlen($keyword), $keyword, $input);
        }
        return $result;
    }
}


$input  = ' NULL OR 1 = 1; DROP TABLE usuarios; INSERT INTO //';
$vulnerabilitySearcher = new ComposedVulnerabilitySearcher([
    new KeyWordsVulnerabilitySearcher(['DROP TABLE']),
    new KeyWordsVulnerabilitySearcher(['INSERT INTO']),
]);

$vulnerabilities = $vulnerabilitySearcher->search($input);
var_dump($vulnerabilities);

$vulnerabilities = $vulnerabilitySearcher->search('john');
var_dump($vulnerabilities);
