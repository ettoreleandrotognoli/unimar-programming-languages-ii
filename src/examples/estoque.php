
<?php

class Produto {
    public function __construct(
        public string $nome
    ) {
    }


    public function __toString() : string {
        return $this->nome;
    }
    
}

class LimiteEstoque {

    public function __construct(
        public int $limite = 3
    )
    {
        
    }

    function verificar(Produto $produto, int $quantidade) {
        if($quantidade <= $this->limite) {
            echo "Estoque do produto ", $produto, " está baixo\n";
        }
    }
}

class Estoque {

    public function __construct(
        public Produto $produto,
        public LimiteEstoque $limite,
        private int $quantidade = 0
    )
    {
        
    }

    public function consumir($quantidade) {
        if($this->quantidade <= $quantidade) {
            throw new Exception("Estoque insuficiente");
        }
        $this->quantidade -= $quantidade;
        $this->limite->verificar($this->produto, $this->quantidade);
    }
}




$leite = new Produto('Leite');

$estoqueDeLeite = new Estoque($leite, new LimiteEstoque(3), 15);

var_dump($estoqueDeLeite);

$estoqueDeLeite->consumir(10);
$estoqueDeLeite->consumir(3);

var_dump($estoqueDeLeite);
