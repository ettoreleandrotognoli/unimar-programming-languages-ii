<?php

use PHPUnit\Framework\TestCase;

final class CalculadoraTest extends TestCase
{


    public function setUp(): void
    {
        $this->calculadora = new Projeto01\Calculadora();
    }

    public function testSoma()
    {
        $this->assertEquals($this->calculadora->soma(0, 0), 0);
    }

    public function testElementoNeutro()
    {
        for ($i = 0; $i < 100; $i += 1) {
            $number = rand();
            $result = $this->calculadora->soma($number, 0);
            $this->assertEquals($number, $result);
        }
    }


    public function testComplemento() {
        for ($i = 0; $i < 100; $i += 1) {
            $number = rand();
            $result = $this->calculadora->soma($number, -$number);
            $this->assertEquals($result, 0);
        }
    }
}
