<?php

class Pessoa {
    public $nome;

    function __construct(string $nome) {
        $this->nome = $nome;
    }

    function __toString() : string {
        return "Pessoa { nome: $this->nome }";
    }
}