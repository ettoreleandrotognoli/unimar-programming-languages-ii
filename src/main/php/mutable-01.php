<?php

interface Named {

    function getName() : string;
}

interface MutableNamed extends Named {

    function setName(string $name) : void;
}


function hello(Named $named) {
    echo 'Hello ', $named->getName();
    # $named->setName();
}