<?php

function sub($a, $b) {
    return $a - $b;
}

function soma($a, $b) {
    return $a + $b;
}

# $fn = sub;
$fn = 'sub';

echo $fn(4 , 5) , "\n";

$fn = 'soma';

echo $fn(2, 2), "\n";