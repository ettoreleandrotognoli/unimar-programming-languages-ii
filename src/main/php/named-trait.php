<?php

trait NamedTrait {

    public function setName(string $name) : void {
        $this->name = $name;
    }

    public function getName() : string {
        return $this->name;
    }

}

class Label {
    use NamedTrait;

    function __construct(
        protected string $name
    ) {

    }
}

$label = new Label("traits \o/");
echo $label->getName(),"\n";