<?php

interface Named {
    function getName() : string;
}

class Label implements Named {
    public function getName() : string { return 'Label name'; }
}

class Tag implements Named {
    public function getName() : string { return 'Tag name'; }
}

function hello(Named $named) {
    echo 'Hello ', $named->getName();
}