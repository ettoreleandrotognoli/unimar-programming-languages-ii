<?php
class A {
    public $value = 0;
}

function set(A $a) {
    $a->value = 1;
}

$a = new A();

var_dump($a);
set($a);
var_dump($a);
