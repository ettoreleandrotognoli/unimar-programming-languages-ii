<?php

function xrange($start, $end, $step = 1)
{
    for ($i = $start; $i <= $end; $i += $step) {
        yield $i;
    }
}

var_dump(range(0, 9)); # eager
var_dump(xrange(0, 9)); # lazy