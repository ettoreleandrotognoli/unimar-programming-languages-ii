<?php

class Pessoa {
    private $nome = 'fuu';
    function __toString() {
        return $this->nome;
    }
}

$pessoa = new Pessoa();
$class = new \ReflectionClass('Pessoa');
$property = $class->getProperties()[0];
$property->setAccessible(true);
$property->setValue($pessoa, 'ettore');
var_dump($pessoa);
$pessoa->nome = 'test';
