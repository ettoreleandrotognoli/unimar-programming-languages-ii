<?php

#[Attribute(Attribute::TARGET_PROPERTY)]
class Column {
    public function __construct(
        public string $name
    ) {}
    public function __toString() {
        return "Coloumn($this->name)";
    }
}

class Pessoa {
    #[Column("nome")]
    public $nome;
}

$class = new ReflectionClass(Pessoa::class);
foreach($class->getProperties() as $property) {
    var_dump($property->name);
    foreach($property->getAttributes() as $attribute) {
        var_dump($attribute->newInstance());
    }
}