<?php

function fatorial($number) {
    if($number < 0) {
        throw new \Exception();
    }
    if($number <= 1) {
        return 1;
    }
    return $number * fatorial($number -1);
}

var_dump(fatorial(5));