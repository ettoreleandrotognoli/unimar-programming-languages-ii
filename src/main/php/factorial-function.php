<?php

function factorial($number) {
    if($number < 0) {
        throw new \Exception();
    }
    $result = 1;
    for(;$number>1;$number-=1) {
        $result *= $number;
    }
    return $result;
}

var_dump(factorial(5));