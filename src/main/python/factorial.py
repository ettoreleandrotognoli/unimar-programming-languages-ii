
from unittest import result


def factorial(number):
    if number < 0:
        raise Exception()
    result = 1
    for value in range(number, result, -1):
        result *= value
    return result


factorial(5)