def factorial(number):
    if number < 0:
        raise Exception()
    if number <= 1:
        return 1
    return number * factorial(number -1)

factorial(5)