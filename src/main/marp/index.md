---
marp: true
theme: default
---

<style>
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    opacity: 0.2;
}
section.title h1 {
  font-size: 55px;
}
</style>

<!-- _class: invert cover title center -->
<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->

# Linguagem de Programação II

Prof. Ettore Leandro Tognoli

---

<!-- paginate: true -->

![bg fit](./resources/qrcode.svg)

<!-- _footer: https://ettoreleandrotognoli.gitlab.io/unimar-programming-languages-ii/ -->

---

<style scoped>
p {
    font-size: 20px
}
</style>

# Sobre Mim

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

![bg right fit](./resources/me.jpg)

[![Github Badge](https://img.shields.io/badge/--blue?style=social&logo=github&link=https://github.com/ettoreleandrotognoli)](https://github.com/ettoreleandrotognoli)[![Gitlab Badge](https://img.shields.io/badge/--blue?style=social&logo=gitlab&link=https://gitlab.com/ettoreleandrotognoli)](https://gitlab.com/ettoreleandrotognoli)[![Linkedin Badge](https://img.shields.io/badge/--blue?style=social&logo=linkedin&link=https://www.linkedin.com/in/ettore-leandro-tognoli/)](https://www.linkedin.com/in/ettore-leandro-tognoli/)[![Discord Badge](https://img.shields.io/badge/--blue?style=social&logo=discord&link=https://discord.gg/Mr6yxp4ZE2)](https://discord.gg/Mr6yxp4ZE2)[![Telegram Badge](https://img.shields.io/badge/--blue?style=social&logo=telegram&link=https://t.me/ettoreleandrotognoli)](https://t.me/ettoreleandrotognoli)[![Instagram Badge](https://img.shields.io/badge/--blue?style=social&logo=instagram&link=https://www.instagram.com/ettoreleandrotognoli/)](https://www.instagram.com/ettoreleandrotognoli/)[![Twitter Badge](https://img.shields.io/badge/--blue?style=social&logo=twitter&link=https://twitter.com/EttoreTognoli)](https://twitter.com/EttoreTognoli)

---

<!-- header: Sobre mim -->

![bg](./resources/ada.jpg)
![bg](./resources/git.jpg)
![bg](./resources/lua.jpg)
![bg](./resources/rust.jpg)
![bg](./resources/shell.jpg)
![bg](./resources/fedora.jpg)
![bg](./resources/web.jpg)

<!-- _footer: Ada :cat:, Git :cat:, Lua :cat:, Rust :dog:, Shell :cat:, Fedora :dog: e a Web :cat: -->

---

## Formação

Bacharelado em Ciência da Computação - UNIVEM ( 2012 )

Mestrado em Ciência da Computação - UFSCar ( 2016 )

## Experiência

Programador/Desenvolvedor em algumas empresas em Marília (2011)

Consultor e Programador Freelancer ( 2014 )

Professor de Graduação - FATEC Pompeia ( 2018/2019 )

Professor de Pós-Graduação - UNIVEM (2017/2018)

Engenheiro de Software - Red Hat ( 2020 )

<!-- Meu ponto de vista -->

---

<!-- header: "" -->

# Bibliografia

<style scoped>
p {
    font-size: 20px
}
</style>

![bg right](./resources/barts-blackboard-i-must-not-obey.jpg)

Vozes da minha cabeça e:

Código Limpo - Robert C. Martin
O Codificador Limpo - Robert C. Martin
Arquitetura Limpa - Robert C. Martin
Desenvolvimento Ágil Limpo - Robert C. Martin
Padrões de Projeto - Erich Gamma
Domain Driven Design - Erich Evans

---

# Material de apoio/complementar

[Coding Better World Together - Uncle Bob](https://www.youtube.com/watch?v=7EmboKQH8lM&list=PLmmYSbUCWJ4x1GO839azG_BBw8rkh-zOj)

[Design Patterns - Christopher Okhravi](https://www.youtube.com/watch?v=v9ejT8FO-7I&list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc)

[Docker Tutorial for Beginners - TechWorld with Nana](https://www.youtube.com/watch?v=3c-iBn73dDEs)

[Red Hat e-books](https://developers.redhat.com/e-books)

---

# Desenvolvimento de uma Biblioteca

![bg right](./resources/pinky-and-the-brain-typing.gif)

Vocês vão escolher o tema,
vão utilizá-la nas aulas de Desenv. Aplicações Web,
vão modela-la nas aulas de Engenharia de Software
e vão implementa-lá todas as noites

<!-- _footer: https://pt.wikipedia.org/wiki/Biblioteca_(computa%C3%A7%C3%A3o) -->

---

## Motivação

Bibliotecas são casos interessantes para aplicação de TDD e é uma programação pensada para reúso, afinal, seus clientes são outros desenvolvedores.

---

## Objetivo

Não só usar linguagens de programação, mas apresentar o contexto em que elas estão inseridas.

<!-- _footer: https://roadmap.sh/ -->

---

<style scoped>
ul {
    font-size: 20px
}
</style>

## Sumário

- Apresentação/Introdução

  - [Sobre Mim](#3)
  - [Avaliação](#13)

- Ambiente

  - Linux
  - Gerenciamento de código fonte
  - Containers

- Linguagem de Programação

  - PHP
  - Funcional
  - Orientação a Objetos
  - PHP "Avançado"

- CI/CD

---

<!-- _class: invert -->
<!-- header: Avaliação -->

# Avaliação

---

Não teremos provas, mas teremos entregas constantes
Porém preciso lançar notas no sistema :facepalm:

- **P1** Entre 26/09 e 06/10
- **P2** Entre 23/11 e 02/12
- **Sub** Entre 05/12 e 07/12
- **Exame** Entre 12/12 e 19/12

---

O que vai contar nota?

- Mensagens dos commits
- Limpeza do código
- Testes
- Funcionamento
- Notas de Aula Versionadas

---

<!-- _class: invert -->
<!-- header: Ambiente -->

# Ambiente

---

# Linux

GNU/LINUX

Assistam [Revolution OS](https://www.imdb.com/title/tt0308808/) e/ou ["The Weird History of Linux"](https://www.youtube.com/watch?v=ShcR4Zfc6Dw)

![bg fit right](resources/tux.svg)

<!-- _footer: https://en.wikipedia.org/wiki/List_of_Linux_distributions -->

## <!-- Cinema na UNIMAR quinta-feira -->

![bg contain right](resources/gnu-logo.svg)

GNU - GNU's not unix

Sistemas UNIX

---

Usem linux
É um diferencial ( exigência ) no currículo
Windows é para jogos :sweat_smile:
Podem fazer um dual boot

---

<!-- _class: invert -->

# Básico para o desenvolvedor

---

# Terminal/Console

CLI
Não é simplesmente cliente

Command Line Interface

`--` Opção com nome completo
`-` Atalho de opção

A maioria dos comandos tem a opção `--help`, também podemos usar o comando `man`.

```sh
man bash
```

---

# Terminais

[shell](https://pt.wikipedia.org/wiki/Shell_script)
[bash](https://pt.wikipedia.org/wiki/Bash)
[ash](https://en.wikipedia.org/wiki/Almquist_shell)
[zshell](https://pt.wikipedia.org/wiki/Z_shell)

---

# Atalhos

`tab` para autocompletar

Tecla direcional para cima e para baixo navegam no histórico

`ctrl` + `r` procura no histórico

`ctrl` + `shift` + `c` para copiar

`ctrl` + `shift` + `v` para colar

---

# Navegação

Onde estou?

```sh
pwd
```

Listar arquivos

```sh
ls
ls -la
```

---

# Navegação

Mudar de Diretório

```sh
cd
cd /
cd ~
cd ..
cd pasta
```

Criar pasta

```sh
mkdir pasta
mkdir -p pasta
```

---

# Comandos Úteis

[echo](<https://pt.wikipedia.org/wiki/Echo_(comando)>)
[cat](<https://pt.wikipedia.org/wiki/Cat_(Unix)>)
[tail](https://pt.wikipedia.org/wiki/Tail)
[head](<https://en.wikipedia.org/wiki/Head_(Unix)>)
[grep](https://pt.wikipedia.org/wiki/Grep)
[diff](https://pt.wikipedia.org/wiki/Diff)
[ps](<https://pt.wikipedia.org/wiki/Ps_(Unix)>)

---

# Entrada e saída

![bg fit right](resources/shell-pipeline.svg)

Redirecionar a saída `>`

Redirecionar a entrada `<`

"Piping" `|`

```sh
echo 'teste' > file
```

```sh
cat file | grep 't'
```

```sh
cat file | grep 't' > out.txt
```

<!-- _footer: https://en.wikipedia.org/wiki/Pipeline_(Unix) -->

---

# Editores de texto no terminal

[nano](<https://pt.wikipedia.org/wiki/GNU_nano_(editor_de_texto)>)
[vi](https://pt.wikipedia.org/wiki/Vi)
[vim](https://pt.wikipedia.org/wiki/Vim)

---

```sh
nano file
```

Para escrever é só digitar normalmente

Salvar `ctrl` + `o`
Sair `ctrl` + `x`

---

```sh
vi file
```

Para escrever habilite o modo insert primeiro apertando `i`
Aperte `esc` para sair do modo insert
Salvar `:w`
Sair `:q`
Salva e sair: `:wq`

---

# Sistema de Permissões

Todos os usuários tem um id de usuário e podem pertencer a `n` grupos.

Quem sou eu?

```sh
whoami
```

---

Mais informações

```sh
id
```

```sh
id -u
```

```sh
id -un
```

---

# Permissões em arquivos

Todo arquivo pertence a um usuário e a um grupo
Uma pasta também é um arquivo
Podemos dar permissão de escrita, leitura e execução para o usuário dono, grupo dono ou para outros ( qualquer um que não seja o dono ou não seja do grupo).

---

|        |          |     |     |
| ------ | -------- | --- | --- |
| Dono   | Leitura  | r   | 4   |
|        | Escrita  | w   | 2   |
|        | Execução | x   | 1   |
| Grupo  | Leitura  | r   | 4   |
|        | Escrita  | w   | 2   |
|        | Execução | x   | 1   |
| Outros | Leitura  | r   | 4   |
|        | Escrita  | w   | 2   |
|        | Execução | x   | 1   |

---

Exemplos

```sh
chmod 777 file
```

```sh
chmod 600 file
```

```sh
chmod u+w file
```

```sh
chmod g-w file
```

```sh
chmod o-rwx file
```

---

# Filesystem Hierarchy Standard (FHS)

- `/` Diretório raiz de toda a hierarquia do sistema de arquivos.
- `/bin/` Comandos binários essenciais para todos os usuários (ex: cat, ls, cp)
- `/boot` Arquivos do Boot loader (ex: núcleo, initrd).
- `/dev/` Dispositivos (ex: /dev/null).
- `/etc/` Arquivos de configuração específicos do computador.
- `/home/` Diretórios de usuários.
- `/root/` Diretório home para o superusuário (root).
- `/tmp/` Arquivos temporários.

## <!-- _footer: https://pt.wikipedia.org/wiki/Filesystem_Hierarchy_Standard -->

---

# Máquinas Virtuais

[Boxes](https://wiki.gnome.org/Apps/Boxes)
[Virtual Box](https://www.virtualbox.org/)
[VMware](https://www.vmware.com/products/workstation-player.html)
[libvirt](https://libvirt.org/)

<!-- _footer: https://pt.wikipedia.org/wiki/M%C3%A1quina_virtual -->

---

Testem diferentes distros usando máquinas virtuais

Escolham uma distro

Instalem nas suas máquinas pessoais

<!-- _footer: https://www.edivaldobrito.com.br/linus-torvalds-revelou-as-especificacoes-de-hardware-do-seu-pc/ -->

---

![bg right fit](./resources/red-hat-logo.svg)

[Red Hat Single Sign-On](https://sso.redhat.com)

[Red Hat Academy](https://rha.ole.redhat.com/rha/app/academy)

---

<!-- header: IDE -->
<!-- _class: invert -->

# IDEs

Integrated Development Environment

Gratuitos

<https://netbeans.apache.org/>
<https://code.visualstudio.com/>

Pagos
<https://www.sublimetext.com/>
<https://www.jetbrains.com/phpstorm/>

<!-- _footer: https://en.wikipedia.org/wiki/Integrated_development_environment -->

---

<!-- _class: invert -->
<!-- header: git -->

# Gerenciamento de Código Fonte

[GIT](https://git-scm.com/)
[Mercurial](https://www.mercurial-scm.org/)
[SVN](https://subversion.apache.org/)

![bg fit right](resources/git-logo.svg)

<!-- _footer: https://git-scm.com/book/pt-br/v2 -->

---

GIT pode ser usado individualmente, mas ele é essencial para o desenvolvimento em time.
Cada colaborador pode trabalhar sem medo de estragar o código dos outros, mas para isso geralmente são definidos alguns processos.

![bg fit right](resources/git-flow.png)

<!-- _footer: https://nvie.com/posts/a-successful-git-branching-model/ -->

---

## Instalação

```sh
sudo dnf install -y git git-gui
```

Configuração Inicial

```sh
git config --global user.name "Fulano de Tal"
git config --global user.email "fulanodetal@exemplo.br"
git config --global init.defaultBranch main
```

---

"O GitLab é um gerenciador de repositório de software baseado em git, com suporte a Wiki, gerenciamento de tarefas e CI/CD. GitLab é similar ao GitHub, mas o GitLab permite que os desenvolvedores armazenem o código em seus próprios servidores, ao invés de servidores de terceiros."

<https://gitlab.com/>

![bg right fit](resources/gitlab-logo.svg)

<!-- _footer: https://pt.wikipedia.org/wiki/GitLab -->

---

"GitHub é uma plataforma de hospedagem de código-fonte e arquivos com controle de versão usando o Git. Ele permite que programadores, utilitários ou qualquer usuário cadastrado na plataforma contribuam em projetos privados e/ou Open Source de qualquer lugar do mundo."

<https://github.com/>

![bg right fit](resources/github-logo.svg)

<!-- _footer: https://pt.wikipedia.org/wiki/GitHub -->

---

Criem um repositório para suas notas de aula e etc.

---

<!-- header: markdown -->
<!-- _class: invert -->

# Markdown

---

```md
# Title 1

## Title 2

## Title 3

...

1. Tópico 1
1. Tópico 2
```

---

# Title 1

## Title 2

## Title 3

...

1. Tópico 1
1. Tópico 2

---

<https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>
<https://dev.to/envoy_/mastering-markdown-the-complete-guide-2dmf>
<https://roachhd.gitbooks.io/github-guides/content/markdown/markdown.html>

---

<!-- header: Containers -->
<!-- _class: invert -->

# Containers

[Docker](https://www.docker.com/)
[Podman](https://podman.io/)
[Kubernetes](https://kubernetes.io/)
[OpenShift](https://www.redhat.com/en/technologies/cloud-computing/openshift)

![bg contain right](resources/docker-logo.svg)

---

Acabar com a frase "na minha máquina funciona"

Simplificar o processo de deploy

Facilita o testes de diferentes ferramentas, ou da mesma ferramenta com diferentes versões.

Frequentemente comparado com máquinas virtuais.

---

![bg fit](resources/virtualization-vs-containers.png)

<!-- _footer: https://www.redhat.com/pt-br/topics/containers/containers-vs-vms -->

---

<!-- _class: invert -->

# O básico para o desenvolvedor

---

## Instalação

```sh
sudo dnf install -y docker
```

ou

```sh
sudo dnf install -y podman
```

---

O docker usa um [deamon](<https://pt.wikipedia.org/wiki/Daemon_(computa%C3%A7%C3%A3o)>) e cria um arquivo socket `/var/run/docker.sock`

```sh
$ ls -la /var/run/docker.sock
srw-rw----. 1 root docker 0 Jul 16 11:31 /var/run/docker.sock
```

```sh
sudo usermod -aG docker $(id -un)
```

---

## Hello World

```sh
docker run -it --rm hello-world
```

`-it` deixa o container iterativo

`--rm` para remover o container após o uso

---

## Distros

```sh
docker run -it --rm alpine ash
```

```sh
docker run -it --rm fedora bash
```

```sh
docker run -it --rm ubuntu bash
```

---

# Bancos

[MySQL](https://hub.docker.com/_/mysql)

```sh
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:tag
```

[Postgres](https://hub.docker.com/_/postgres/)

```sh
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

[Mongo](https://hub.docker.com/_/mongo/)

```sh
docker run -d --name some-mongo \
	-e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
	-e MONGO_INITDB_ROOT_PASSWORD=secret \
	mongo
```

---

- Containers
  `docker ps -a`
- Imagens
  - [alpine](https://hub.docker.com/_/alpine)
  - [python](https://hub.docker.com/_/python)
  - [nginx](https://hub.docker.com/_/nginx)
  - [php](https://hub.docker.com/_/php)
- Repositórios
  - [hub.docker.com](https://hub.docker.com)
  - [quay.io](https://quay.io/)

<!-- _footer: https://dockerlabs.collabnix.com/docker/cheatsheet/ -->

---

# Docker & Redes

- bridge ( padrão )
- host ( vinculada com o host)
- outras
  - macvlan
  - ipvlan
  - overlay
  - none

<!-- _footer: https://www.youtube.com/watch?v=bKFMS5C4CG0 -->

---

# Portas

Quando usamos `host` as portas do container são expostas no próprio host.

Quando usamos outro tipo, `bridge` por exemplo, precisamos nos conectar com o IP do container ou expor a porta no host.

```sh
docker run -itd --name web-server -p 8080:80 nginx
firefox localhost:8080 &!
docker inspect web-server
firefox $(docker inspect -f '{{ range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' web-server) &!
firefox $(docker inspect web-server | jq -r  ".[0].NetworkSettings.Networks.bridge.IPAddress") &!
docker rm -f web-server
```

---

# Volumes

Por padrão os containers são efêmeros, então como persistimos os dados?

```sh
docker run -d \
	--name some-postgres \
	-e POSTGRES_PASSWORD=mysecretpassword \
	-v /host/path:/var/lib/postgresql/data \
	postgres
```

```sh
docker run -d \
	--name some-postgres \
	-e POSTGRES_PASSWORD=mysecretpassword \
	-v volume-name:/var/lib/postgresql/data \
	postgres
```

---

# Volumes

## E o Mysql?

```sh
docker run -d \
    --name some-mysql \
    -v /my/own/datadir:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=my-secret-pw \
    mysql:tag
```

---

# docker-compose

![bg right fit](./resources/docker-compose-logo.png)

```sh
pip install --user docker-compose
```

---

<!-- embedme src/phpmyadmin/docker/docker-compose.yaml -->

```yaml
version: "3.1"

services:
  db:
    image: mysql
    restart: always
    volumes:
      - ${PWD}/mysql-data:/var/lib/mysql
    ports:
      - 3306:3306
    environment:
      MYSQL_ROOT_PASSWORD: notSecureChangeMe

  phpmyadmin:
    image: phpmyadmin
    restart: always
    ports:
      - 8080:80
    environment:
      - PMA_ARBITRARY=1
```

---

```sh
docker-compose up
docker-compose up -d
docker-compose -f docker-compose.yaml up -d
```

---

# CLOUD

Para o deploy/produção são usados clusters, como kubernetes ( k8s ), openshift, docker swarm e etc.

![bg right fit](resources/Cloud-Computing.webp)

<!-- _footer: https://ostechnix.com/cloud-computing-basics/ -->

"CLOUD é só o computador de outra pessoa"

Containers são fáceis de replicar, tem tempo de inicialização muito menor do que o de VMs.

---

<!-- _class: invert -->

# Linguagens de Programação II

Agoras sim vamos começar!

Vamos utilizar a linguagem de programação PHP para exemplificar os conceitos, mas nunca se esqueça que os conceitos podem ser reutilizados para o aprendizado de outras linguagens.

Aprenda bem a linguagem, seus recursos, sua sintaxe, suas peculiaridades e etc, mas não se apegue. Linguagens de programação sãos apenas ferramentas, mais cedo ou mais tarde você vai precisar trocar de ferramenta para fazer um diferente trabalho.

---

# Instalando PHP & CIA

![bg fit right](resources/php-elephant.svg)

<https://www.php.net/>
<https://getcomposer.org/>

---

Instalação do PHP

```sh
sudo dnf install -y php php-json php-xml php-mbstring
```

Instalação do Composer

```sh
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
```

---

<!-- _class: invert -->
<!-- header: PHP -->

![bg fit right](resources/php-elephant.svg)

# PHP

<!-- _footer: https://www.php.net/manual/en/langref.php -->

---

# Olá Mundo

```php
<?php

echo "Hello World";
```

```sh
php file.php
```

---

```sh
git init
git add .
git commit -m "Hello World"
```

```
git log
```

```
git commit --amend
```

![bg right fit](resources/git-logo.svg)

---

# Remotos

![bg right:70% fit](resources/git-remotes-uml.svg)

---

## Usando SSH

```sh
git remote add origin git@gitlab.com:ettoreleandrotognoli/unimar-programming-languages-ii.git
```

## Usando HTTPs

```sh
git remote add origin https://gitlab.com/ettoreleandrotognoli/unimar-programming-languages-ii.git
```

---

Listar remotos

```sh
git remote -v
```

Remover remoto

```sh
git remote remove <remote>
```

Mudar url de um remoto

```sh
git remote set-url <remote> <url>
```

---

# SSH

"Secure Shell (SSH) é um protocolo de rede criptográfico para operação de serviços de rede de forma segura sobre uma rede insegura. O melhor exemplo de aplicação conhecido é para login remoto de utilizadores a sistemas de computadores. "

É necessário configurar um par de chaves criptográficas <https://docs.gitlab.com/ee/user/ssh.html>

```sh
ssh-keygen -t rsa -b 2048
```

<!-- _footer: https://pt.wikipedia.org/wiki/Secure_Shell -->

<!-- Usa porta do SSH, talvez tenha problemas com firewalls -->

---

# HTTPs

"HTTPS (Hyper Text Transfer Protocol Secure - protocolo de transferência de hipertexto seguro) é uma implementação do protocolo HTTP sobre uma camada adicional de segurança que utiliza o protocolo SSL/TLS."

Vai sempre pedir usuário e senha

<!-- _footer: https://pt.wikipedia.org/wiki/Hyper_Text_Transfer_Protocol_Secure -->

<!-- Usa porta do HTTPs, menos problemas com firewalls -->

---

Enviando modificações

```
git push <remote> <branch>
```

```sh
git push origin main
```

---

Recebendo Modificações

```
git pull <remote> <branch>
```

```sh
git pull origin main
```

Usando rebase como estratégia de mesclagem

```sh
git pull origin main --rebase
```

---

<!-- _class: invert -->

# De volta ao PHP

---

Nasceu como uma linguagem de "template"

```
nano index.php
```

<!-- embedme src/main/php/index.php -->

```php
<?php
$title = 'Título da Página';
$body = 'Hello World';
?>
<html>
<head>
    <title><?php echo $title; ?></title>
</head>
    <body>
        <?php echo $body; ?>
    </body>
</html>
```

```sh
php index.php
```

---

Hoje tem até um servidor embutido

```sh

php -S 0.0.0.0:8000

```

Agora basta acessar com o browser <http://localhost:8000>

<!-- _footer: https://www.php.net/manual/en/features.commandline.webserver.php -->

---

![bg right fit](./resources/rasmus-lerdorf.jpg)

# Rasmus Lerdorf

PHP - Personal Home Page Tools

<!-- _footer: https://pt.wikipedia.org/wiki/Rasmus_Lerdorf -->

---

```sh
git add index.php
git commit
```

```sh
nano README.md
```

```md
# Linguagem de Programação II

Usar `php -S 0.0.0.0:8000` para iniciar o servidor
```

```sh
git add README.md
git commit
git push origin main
```

---

# Variáveis

<!-- header: PHP & Variáveis -->

Variáveis são espaços nomeados e reservados na memória
Cada variável armazena apenas um valor por vez

<!-- embedme src/main/php/variables.php -->

```php
<?php

$variavel = 1;
$variavel = '1';
$variavel = "$variavel";
```

<!-- _footer: https://www.php.net/manual/en/language.variables.php -->

---

# Inspecionar variável

Função `var_dump`

<!-- embedme src/main/php/variables-dump.php -->

```php
<?php

$variavel = 1;
var_dump($variavel);
$variavel = '1';
var_dump($variavel);
$variavel = "$variavel";
var_dump($variavel);
```

<!-- _footer: https://www.php.net/manual/en/function.var-dump.php -->

---

# Nomear variáveis

Significativos
Pronunciáveis
Passíveis de busca
Quanto menor o escopo, mais especifico, menos informações são necessárias nos nomes.
Quanto maior o escopo, mais informações são necessárias.

---

![bg right:40%](./resources/hiding-homer.gif)

```php
<?php
$a = [ 10, 7, 5, 8, 9];
$b = array_sum($a);
$c = $b / count($a);
echo $c;
```

---

![bg right:40%](./resources/homer-disappointed.jpg)

```php
<?php
// notas
$a = [ 10, 7, 5, 8, 9];
// soma das notas
$b = array_sum($a);
// media das notas
$c = $b / count($a);
echo $c;
```

---

![bg right:40%](./resources/excited-homer.gif)

```php
<?php
$notas = [ 10, 7, 5, 8, 9];
$soma = array_sum($notas);
$media = $soma / count($notas);
echo $media;
```

---

# Tipagem

Cada variáveis tem um tipo e seu tipo determina seu comportamento e tamanho na memoria.

- bool ( true, false)
- int
- float
- string

<!-- _footer: https://www.php.net/manual/en/language.types.intro.php -->

---

# Operações com Booleanos / Lógicas

|              |     |                                                |
| ------------ | --- | ---------------------------------------------- |
| `$a and $b`  | And | true if both $a and $b are true.               |
| `$a or $b`   | Or  | true if either $a or $b is true.               |
| `$a xor $b`  | Xor | true if either $a or $b is true, but not both. |
| `! $a`       | Not | true if $a is not true.                        |
| `$a && $b`   | And | true if both $a and $b are true.               |
| `$a \|\| $b` | Or  | true if either $a or $b is true.               |

<!-- _footer: https://www.php.net/manual/en/language.operators.logical.php -->

---

# Quando é falso?

- the boolean false itself
- the integer 0 (zero)
- the floats 0.0 and -0.0 (zero)
- the empty string, and the string "0"
- an array with zero elements
- the special type NULL (including unset variables)
- SimpleXML objects created from attributeless empty elements, i.e. elements which have neither children nor attributes.

<!-- _footer: https://www.php.net/manual/en/language.types.boolean.php -->

---

# Operações com tipos numéricos

<!-- embedme src/main/php/number-operators.php -->

```php
<?php

$a = 2;
$b = 4;
$c = -3;
$delta = $b * $b - 4 * $a * $c;
$x = sqrt($delta / 2 * $a);
echo "Delta = $delta\n";
echo "X = +$x | -$x \n";
```

<!-- _footer: https://www.php.net/manual/en/language.operators.php -->

---

# Operações textos/strings

<!-- embedme src/main/php/text-operators.php -->

```php
<?php

$name = 'World';
$hello = 'Hello, '. $name;
echo $hello;
```

---

# Tipagem

Tipagem fraca e dinâmica
Conversões implícitas

<!-- embedme src/main/php/typing.php -->

```php
<?php

$number = 1;
echo $number * 3, "\n";

$textNumber = '1';
echo $textNumber * 3, "\n";

$text = 'Hello World';
echo $text * 3;
```

<https://www.php.net/manual/en/types.comparisons.php>
<https://www.php.net/manual/en/language.types.type-juggling.php>

---

# Ponteiros/Referências

Compartilham o mesmo endereço/espaço na memoria

<!-- embedme src/main/php/pointer-01.php -->

```php
<?php

$nome = 'Fulano';

$ponteiroParaNome = &$nome;

$ponteiroParaNome .= ' Ciclano';

var_dump($nome);
var_dump($ponteiroParaNome);
```

<!-- _footer: https://www.php.net/manual/en/language.references.php -->

---

# Convenções

|                      |                      |
| -------------------- | -------------------- |
| Camel case           | primeiroNomePessoa   |
| Pascal case          | PrimeiroNomePessoa   |
| Snake case           | primeiro_nome_pessoa |
| Screaming snake case | PRIMEIRO_NOME_PESSOA |
| Kebab case           | primeiro-nome-pessoa |
| Screaming kebab case | PRIMEIRO-NOME-PESSOA |

<!-- _footer: https://www.alura.com.br/artigos/convencoes-nomenclatura-camel-pascal-kebab-snake-case -->

---

# Exercício - Variáveis

Escrevam algum trecho do código do projeto de vocês

O código não precisa ser algo funcional

Os nomes das variáveis devem deixar clara a intenção do código

Se necessário utilizem funções/objetos/classes que ainda nem existem

```sh
git add
git commit
git push
```

---

# Arrays

Na minha época:

<!-- embedme src/main/php/old-init-array.php -->

```php
<?php

$array = array();
var_dump($array);
```

Agora:

<!-- embedme src/main/php/new-init-array.php -->

```php
<?php

$array = [];
var_dump($array);
```

---

<!-- embedme src/main/php/array-as-array.php -->

```php
<?php

$array = [
    1, 2, 3, 4,
];

$array = [
    'fuu',
    'bar',
];

$array = [
    1,
    'bar',
    ['outro array'],
];
```

---

<!-- embedme src/main/php/array-as-map-01.php -->

```php
<?php

$array = [
    0 => 1,
    1 => 2,
    2 => 3,
    3 => 4,
];

$array = [
    0 => 'fuu',
    1 => 'bar',
];

$array = [
    0 => 1,
    1 => 'bar',
    2 => ['outro array'],
];
```

---

<!-- embedme src/main/php/array-as-map-02.php -->

```php
<?php

$array = [
    'a' => 1,
    'b' => 2,
    'c' => 3,
    'd' => 4,
];

```

---

Por muito tempo os arrays foram a base para fazer programas em PHP.

---

# Operações com Arrays

[in_array](https://www.php.net/manual/en/function.in-array.php)
[array_key_exists, key_exists](https://www.php.net/manual/en/function.array-key-exists.php)
[array_values](https://www.php.net/manual/en/function.array-values.php)
[array_keys](https://www.php.net/manual/en/function.array-keys.php)
[array_merge](https://www.php.net/manual/en/function.array-merge.php)
[array_merge_recursive](https://www.php.net/manual/en/function.array-merge-recursive.php)
[extract](https://www.php.net/manual/en/function.extract.php)
[compact](https://www.php.net/manual/en/function.compact.php) + [get_defined_vars](https://www.php.net/manual/en/function.get-defined-vars.php)

<https://www.php.net/manual/en/ref.array.php>

<!-- _footer: https://www.php.net/manual/en/language.operators.array.php -->

---

# JSON

<!-- embedme src/main/php/json-01.php -->

```php
<?php

$array = json_decode('{ "a" : 1, "b" : [] }');
var_dump($array);
echo json_encode($array), "\n";

```

<!-- _footer: https://www.php.net/manual/en/function.json-decode.php -->

---

# Funções

<!-- header: PHP & Funções -->

<!-- embedme src/main/php/function.php -->

```php
<?php

function soma($a, $b) {
    return $a + $b;
}

echo soma(4 , 5);
```

---

# "Ponteiro" de funções

<!-- embedme src/main/php/function-pointer.php -->

```php
<?php

function sub($a, $b) {
    return $a - $b;
}

function soma($a, $b) {
    return $a + $b;
}

# $fn = sub;
$fn = 'sub';

echo $fn(4 , 5) , "\n";

$fn = 'soma';

echo $fn(2, 2), "\n";
```

---

# Regras

Pequenas, mais pequenas
Devem fazer apenas uma coisa
Não devem ter efeitos colaterais
Quanto maior o escopo, menos informações são necessárias nos nomes, pois quanto maior o escopo mais genérica a função deve ser.
Quanto menor o escopo, mais especifico, mais informações são necessárias.

Nome significativos
Pronunciáveis

---

<!-- embedme src/main/php/function-ref-parameter.php -->

```php
<?php

$a = 10;

function soma(&$a, $b) {
    $a += $b;
    return $a;
}

echo soma($a , 5), "\n";
var_dump($a);
```

---

# Função Anonima/Lambda

```php
<?php

$name = 'World';

$helloWorld = function($greetings) use ($name) {
    echo $greetings, ' ', $name, '!!!';
};

$helloWorld('Hello');
```

<!-- _footer: https://www.php.net/manual/en/functions.anonymous.php -->

---

# Programação Funcional

- [array_filter](https://www.php.net/manual/en/function.array-filter.php)
- [array_map](https://www.php.net/manual/en/function.array-map.php)
- [array_reduce](https://www.php.net/manual/en/function.array-reduce.php)

![bg right](./resources/hiding-homer.gif)

---

# Estruturas de Controle

<!-- header: PHP & Estruturas de Controle -->

<!-- embedme src/main/plantuml/control-structure-if-else-uml.puml -->

```plantuml
@startuml

start

if (Saldo suficiente?) then (sim)
  :Efetua compra;
else (não)
  :Cancela compra;
endif

stop

@enduml
```

![bg right fit](./resources/control-structure-if-else-uml.svg)

---

<!-- embedme src/main/php/control-structure-if-else.php -->

```php
<?php

$saldo = 1000;
$preco = 900;

if ($saldo >= $preco) {
    $saldo -= $preco;
    echo "Efetua compra\n";
} else {
    echo "Sem insuficiente\n";
}

echo "Saldo restante ", $saldo, "\n";

```

<!-- _footer: https://www.php.net/manual/en/control-structures.elseif.php -->

---

Não é por que a linguagem permite que você pode/deve fazer código hadouken.

---

![bg fit](resources/hadouken-indentation.webp)

---

```php
function register()
{
    if (!empty($_POST)) {
        $msg = '';
        if ($_POST['user_name']) {
            if ($_POST['user_password_new']) {
                if ($_POST['user_password_new'] === $_POST['user_password_repeat']) {
                    if (strlen($_POST['user_password_new']) > 5) {
                        if (strlen($_POST['user_name']) < 65 && strlen($_POST['user_name']) > 1) {
                            if (preg_match('/^[a-z\d]{2,64}$/i', $_POST['user_name'])) {
                                $user = read_user($_POST['user_name']);
                                if (!isset($user['user_name'])) {
                                    if ($_POST['user_email']) {
                                        if (strlen($_POST['user_email'] < 65)) {
                                            if (filter_var($_POST['user_email'], FILTER_VALIDATE_EMAIL)) {
                                                create_user();
                                                $_SESSION['msg'] = 'You are now registered so please login';
                                                header('Location: '.$_SERVER['PHP_SELF']);
                                                exit();
                                            } else $msg = 'You must provide a valid email address';
                                        } else $msg = 'Email must be less than 64 characters';
                                    } else $msg = 'Email cannot be empty';
                                } else $msg = 'Username already exists';
                            } else $msg = 'Username must be only a-z, A-Z, 0-9';
                        } else $msg = 'Username must be between 2 and 64 characters';
                    } else $msg = 'Password must be at least 6 characters';
                } else $msg = 'Passwords do not match';
            } else $msg = 'Empty Password';
        } else $msg = 'Empty Username';
        $_SESSION['msg'] = $msg;
    }
    return register_form();
}
```

<!-- _footer: https://diogommartins.wordpress.com/2016/08/22/como-se-defender-de-ifs-hadouken/ -->

---

# Switch

<!-- embedme src/main/php/switch-01.php -->

```php
<?php

$i = 3;

switch ($i) {
    case 0:
        echo "i equals 0";
        break;
    case 1:
        echo "i equals 1";
        break;
    case 2:
        echo "i equals 2";
        break;
    default:
        echo $i, " not expected";
}

```

<!-- _footer: https://www.php.net/manual/en/control-structures.switch.php -->

---

Não é por que que a linguagem permite que eu vou abusar

Quando o "mesmo" swicth, if else ... se repetem é uma forte indicação de "mal cheiro".

<!-- _footer: https://en.wikipedia.org/wiki/Code_smell -->

---

# Match

if else, switch com esteróides

<!-- embedme src/main/php/match-01.php -->

```php
<?php

$age = 23;

$result = match (true) {
    $age >= 65 => 'senior',
    $age >= 25 => 'adult',
    $age >= 18 => 'young adult',
    default => 'kid',
};

var_dump($result);
```

<!-- _footer: https://www.php.net/manual/en/control-structures.match.php -->

---

<!-- header: PHP & Estruturas de Repetição -->

# Estruturas de Repetição

![bg right fit](./resources/loop-uml.svg)

---

<!-- embedme src/main/php/while-01.php -->

```php
<?php

$i = 0;

while ($i < 10) {
    var_dump($i);
    $i += 1;
}

```

![bg right fit](./resources/while-uml.svg)

---

<!-- embedme src/main/php/do-while-01.php -->

```php
<?php

$i = 0;
do {
    var_dump($i);
    $i += 1;
} while ($i < 10);

```

![bg right fit](./resources/do-while-uml.svg)

---

<!-- embedme src/main/php/for-01.php -->

```php
<?php

for ($i = 0; $i < 10; $i += 1) {
    var_dump($i);
}

```

---

<!-- embedme src/main/php/foreach-01.php -->

```php
<?php

$array = range(0, 9);

foreach ($array as $value) {
    var_dump($value);
}

```

---

<!-- embedme src/main/php/foreach-02.php -->

```php
<?php

$array = [
    'a' => 1,
    'b' => 2,
    'c' => 3,
];

foreach ($array as $key => $value) {
    var_dump($key, $value);
}

```

---

# Geradores

<!-- _footer: https://www.php.net/manual/en/language.generators.overview.php -->

---

<!-- embedme src/main/php/generator-01.php -->

```php
<?php

function xrange($start, $end, $step = 1)
{
    for ($i = $start; $i <= $end; $i += $step) {
        yield $i;
    }
}

var_dump(range(0, 9)); # eager
var_dump(xrange(0, 9)); # lazy
```

---

# Funções Recursivas

---

# Fatorial Recursivo

`n! = n . (n – 1). (n – 2). (n – 3) ... 2,1`

<!-- embedme src/main/php/recursive-factorial-function.php -->

```php
<?php

function fatorial($number) {
    if($number < 0) {
        throw new \Exception();
    }
    if($number <= 1) {
        return 1;
    }
    return $number * fatorial($number -1);
}

var_dump(fatorial(5));
```

<!-- _footer: https://www.educamaisbrasil.com.br/enem/matematica/fatorial -->

---

# Fatorial não recursivo

`n! = n . (n – 1). (n – 2). (n – 3) ... 2,1`

<!-- embedme src/main/php/factorial-function.php -->

```php
<?php

function factorial($number) {
    if($number < 0) {
        throw new \Exception();
    }
    $result = 1;
    for(;$number>1;$number-=1) {
        $result *= $number;
    }
    return $result;
}

var_dump(factorial(5));
```

---

# Memória Stack vs Memória Heap

<!-- _footer: https://pythontutor.com/ -->

---

<!-- header: PHP -->

`require`
`require_once`
`include`
`include_once`

`include` não falha se não encontrar o arquivo

`require` falha se não encontrar

`once` evita reimportar

---

# PHP 7

<https://www.php.net/manual/en/migration70.new-features.php>

---

# PHP 8

<https://www.php.net/releases/8.0/en.php>

---

# Exercício - Funções

Escrevam algum trecho do código do projeto de vocês

O código precisa ser algo funcional

Os nomes das funções/variáveis/parâmetros devem deixar clara a intenção do código

```sh
git add
git commit
git push
```

---

# Orientação a Objetos

<!-- header: PHP & POO -->

POO - Programação Orientada a Objetos

É um dos muitos paradigmas de linguagens de programação

---

[Alan Kay](https://en.wikipedia.org/wiki/Alan_Kay)

"O computador ideal deve funcionar como um organismo vivo, isso é, cada célula se relaciona com outras a fim de alcançar um objetivo, mas cada uma funciona de forma autônoma. As células poderiam também reagrupar-se para resolver um outro problema ou desempenhar outras funções"

---

Um sistema complexo pode ser apenas a combinação de diversos sistemas simples.

![bg right fit](resources/automato-celular.gif)

<!-- _footer: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life -->

---

Dividir para conquistar

<!-- _footer: https://pt.wikipedia.org/wiki/Dividir_para_conquistar -->

---

Definir comportamento de cada tipo "célula"

Criar as células

Conectar as células

---

# Classes

<!-- embedme src/main/php/classes-01.php -->

```php
<?php

# definição da classe
class MinhaClasse {

}

# instanciação de um objeto
$minhaClasse = new MinhaClasse();
```

![bg right contain](./resources/class-01-uml.svg)

<!-- _footer: https://www.php.net/manual/pt_BR/language.oop5.php -->

---

# Instâncias/Objetos vs Classes/Tipos

Classes Objetos
Meta dados e dados

---

# Atributos

Podemos usar classes para definir estruturas de dados

---

<!-- embedme src/main/php/pessoa-01.php -->

```php
<?php

class Pessoa {
    public $nome;

    function __construct(string $nome) {
        $this->nome = $nome;
    }
}
```

<!-- embedme src/main/plantuml/pessoa-01-uml.puml -->

```puml
@startuml

class Pessoa {
    +nome : string
}

@enduml
```

![bg right fit](resources/pessoa-01-uml.svg)

<!-- _footer: https://plantuml.com/class-diagram -->

---

Comparação com tabelas

| #   | Nome    |
| --- | ------- |
| 0   | Ettore  |
| 1   | Leandro |

Cada linha é uma instância da classe pessoa ( dados )
As colunas são definidas na classe pessoa ( meta dados)

![bg right fit](resources/pessoa-02-uml.svg)

---

# Métodos

Podemos usar classes para definir comportamentos

Normalmente são verbos
Quanto maior o escopo, mais informações são necessárias nos nomes.
Quanto menor o escopo, mais especifico, menos informações são necessárias.

---

<!-- embedme src/main/php/pessoa-02.php -->

```php
<?php

class Pessoa {
    public $nome;

    function __construct(string $nome) {
        $this->nome = $nome;
    }

    function __toString() : string {
        return "Pessoa { nome: $this->nome }";
    }
}
```

---

# UML

![bg right fit](resources/pessoa-03-uml.svg)

---

![bg fit](resources/pessoa-04-uml.svg)

---

<!-- embedme src/main/php/pessoa-03.php -->

```php
<?php

class Pessoa {
    public $nome;

    function __construct(string $nome) {
        $this->nome = $nome;
    }

    function __toString() : string {
        return "Pessoa { nome: $this->nome }";
    }
}

$pessoa0 = new Pessoa('Ettore');
$pessoa1 = new Pessoa('Leandro');

echo $pessoa0, ' ', $pessoa1, "\n";
```

---

# Comportamento vs Dados

---

# Encapsulamento/Visibilidade

- public
- protected
- private

<!-- _footer: https://www.php.net/manual/en/language.oop5.visibility.php -->

---

Como eu testo um método privado?

É possivel acessar usando Reflection

Muitos atributos/métodos privados podem ser movidos para outra classes como públicos

<!-- _footer: https://www.php.net/manual/en/reflectionmethod.setaccessible.php -->

---

# Interfaces

Definem um contrato
A interface deixa explicito o que pode ser feito, não como é feito.
Por padrão tudo em uma interface é público

<!-- embedme src/main/php/interfaces-01.php -->

```php
<?php

interface MyInterface {
    public function method();
}
```

<!-- _footer: https://www.php.net/manual/pt_BR/language.oop5.interfaces.php -->

---

# Named

<!-- embedme src/main/php/interface-named.php -->

```php
<?php

interface Named {
    function getName() : string;
}

class Label implements Named {
    public function getName() : string { return 'Label name'; }
}

class Tag implements Named {
    public function getName() : string { return 'Tag name'; }
}

function hello(Named $named) {
    echo 'Hello ', $named->getName();
}
```

---

![bg fit](resources/named-uml.svg)

---

# Encapsulamento/Visibilidade com interfaces

<!-- embedme src/main/php/mutable-01.php -->

```php
<?php

interface Named {

    function getName() : string;
}

interface MutableNamed extends Named {

    function setName(string $name) : void;
}


function hello(Named $named) {
    echo 'Hello ', $named->getName();
    # $named->setName();
}
```

---

![bg fit](./resources/mutable-01-uml.svg)

---

# Herança

<!-- embedme src/main/php/inheritance-01.php -->

```php
<?php

class A {

}

class B extends A {
    
}
```

![bg right fit](resources/inheritance-01-uml.svg)

---

A classe `B` tem todos os métodos e atributos da classes `A`.
Então qualquer cliente que necessite de um `A` não terá problemas em receber um `B`, nesse sentido bem semelhante ao uso de interfaces.

![bg right fit](resources/inheritance-02-uml.svg)

---

Prefiram composição e delegação no lugar da herança.
Encarem delegação como uma herança dinâmica, ou seja, podemos mudar a herança em tempo de execução.

---

# Herança Estática

![bg right fit](./resources/static-inheritance-01-uml.svg)

---

# Herança Múltipla

Quantas instâncias de `A` fazem parte de uma instância de `D`?

`D` realmente deveria ter acesso a propriedades protegidas de `A`,`B` e `C`?

![bg right fit](./resources/static-inheritance-02-uml.svg)

---

# "Herança" Dinâmica

---

![bg fit](./resources/dynamic-inheritance-01-uml.svg)

---

# Trait

Isso é praticamente exclusivo do PHP.
Temos traits em rust também, mas são usados de forma diferente.
Um jeito "elegante" de copiar e colar código sem usar herança.

![bg right](./resources/excited-homer.gif)

```php

trait Fuu {

}
```

<!-- _footer: https://www.php.net/manual/pt_BR/language.oop5.traits.php -->

---

<!-- embedme src/main/php/named-trait.php -->

```php
<?php

trait NamedTrait {

    public function setName(string $name) : void {
        $this->name = $name;
    }

    public function getName() : string {
        return $this->name;
    }

}

class Label {
    use NamedTrait;

    function __construct(
        protected string $name
    ) {

    }
}

$label = new Label("traits \o/");
echo $label->getName(),"\n";


```

---

# Classes Genéricas

Infelizmente PHP não tem suporte a classes genéricas

![bg right](./resources/sad-homer.gif)

<!-- _footer: https://stitcher.io/blog/generics-in-php-3 -->

---

# Métodos Mágicos

![bg right](./resources/magic.gif)

Basicamente todo método que começar com `__` é considerado um método mágico.
Não tenho ideia do porquê do nome

`__construct()`, `__destruct()`, `__call()`, `__callStatic()`, `__get()`, `__set()`, `__isset()`, `__unset()`, `__sleep()`, `__wakeup()`, `__serialize()`, `__unserialize()`, `__toString()`, `__invoke()`, `__set_state()`, `__clone()`, `__debugInfo()`...

<!-- _footer: https://www.php.net/manual/pt_BR/language.oop5.magic.php -->

---

# Namespaces

<!-- embedme src/main/php/namespace-01.php -->

```php
<?php

namespace git {
    class Repository {}
};

namespace svn {
    class Repository {}
};

namespace {
    $svnRepository = new \svn\Repository();
    $gitRepository = new \git\Repository();
    var_dump($svnRepository);
    var_dump($gitRepository);
};
```

<!-- _footer: https://www.php.net/manual/pt_BR/language.namespaces.php -->

---

Convenções e PSRs ( PHP Standard Recommendation)

<https://en.wikipedia.org/wiki/PHP_Standard_Recommendation>

<!-- _footer: https://www.php-fig.org/psr/psr-4/ -->

---

<!-- embedme src/main/php/autoload-01.php -->

Classe `\git\Repository` no arquivo `git/Repository.php`.

```php
<?php

spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if (file_exists($file)) {
        include_once $file;
        return true;
    }
    return false;
});

```

<!-- _footer: https://www.php.net/manual/en/language.oop5.autoload.php -->

---

# Reflection

A maioria das linguagens interpretadas possuem esse recurso.
Possibilita usar metadados do código em tempo de execução para mudar o comportamento do programa.

![bg right](./resources/homer-mirror.gif)

<!-- _footer: https://www.php.net/manual/en/book.reflection.php -->

---

# Doctrine

```php
<?php
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
}
```

<!-- _footer: https://www.doctrine-project.org/projects/doctrine-orm/en/current/tutorials/getting-started.html -->

---

<!-- embedme src/main/php/reflection-01.php -->

```php
<?php

class Pessoa {
    /**
     * @TODO
     */
    public $nome;
}

$class = new \ReflectionClass('Pessoa');

var_dump($class->getProperties()[0]->getDocComment());
```

---

<!-- embedme src/main/php/reflection-02.php -->

```php
<?php

class Pessoa {
    private $nome = 'fuu';
    function __toString() {
        return $this->nome;
    }
}

$pessoa = new Pessoa();
$class = new \ReflectionClass('Pessoa');
$property = $class->getProperties()[0];
$property->setAccessible(true);
$property->setValue($pessoa, 'ettore');
var_dump($pessoa);
$pessoa->nome = 'test';

```

---

# PHP 8 & Attributes

Atributos == Anotações

<!-- _footer: https://stitcher.io/blog/attributes-in-php-8 -->

---

<!-- embedme src/main/php/attribute-01.php -->

```php
<?php

#[Attribute(Attribute::TARGET_PROPERTY)]
class Column {
    public function __construct(
        public string $name
    ) {}
    public function __toString() {
        return "Coloumn($this->name)";
    }
}

class Pessoa {
    #[Column("nome")]
    public $nome;
}

$class = new ReflectionClass(Pessoa::class);
foreach($class->getProperties() as $property) {
    var_dump($property->name);
    foreach($property->getAttributes() as $attribute) {
        var_dump($attribute->newInstance());
    }
}
```

---

# Gerenciamento de Dependências

[Composer](https://getcomposer.org/)

![bg fit right](resources/composer-logo.svg)

<!-- _footer: https://github.com/composer/composer -->

---

```sh
composer init
composer install
```

- composer.json
- composer.lock
- vendor
  - composer
  - autoload.php

---

```sh
composer require --dev phpunit/phpunit ^9
```

<!-- embedme src/project-01/php/composer.json -->

```json
{
    "name" : "unimar/project-01",
    "version": "0.0.0",
    "require-dev": {
        "phpunit/phpunit": "^9"
    },
    "autoload": {
        "psr-4": {
            "Projeto01\\": "src/"
        }
    },
    "scripts": {
        "test" : "vendor/bin/phpunit --bootstrap vendor/autoload.php tests"
    }
}
```

---

# Versionamento Semântico


## MAJOR.MINOR.PATCH
## MAJOR.MINOR.MICRO

MAJOR -> mudanças incompatíveis na API
MINOR -> novas funcionalidades mantendo compatibilidade
PATCH/MICRO -> correção de falhas mantendo compatibilidade


<!-- _footer: https://semver.org/lang/pt-BR/ -->

---

PHP unit e ~~Testes unitários~~ / Testes de unidade

---

<!-- embedme src/project-01/php/tests/HelloTest.php -->

```php
<?php

// src/project-01/php/tests/HelloTest.php 

use PHPUnit\Framework\TestCase;

final class HelloTest extends TestCase
{
    public function testHello() {
        $this->assertEquals("hello", "hello");
    }
}

```

```sh
composer test
```

---

<!-- _class: invert -->
<!-- header: Qualidade -->

# Qualidade

---

# Clean Code & TDD

[Robert C. Martin (Tio Bob)](https://en.wikipedia.org/wiki/Robert_C._Martin)

["Coding Better World Together" 2019](https://www.youtube.com/watch?v=7EmboKQH8lM&list=PLmmYSbUCWJ4x1GO839azG_BBw8rkh-zOj)

---

# Legibilidade é Importante

"um programador passa 10 vezes mais tempo lendo código do que escrevendo"

---

# Os dois valores

Software nãoe é hardware

"Soft" significa fácil de mudar

Comportamento e Estrutura

![bg right fit](./resources/clean-code-cost.png)

<!-- _footer: https://dev.to/ruppysuppy/5-tips-to-master-the-art-of-clean-code-57b6 -->

---

# Com TDD é mais Rápido

Mesmo escrevendo "tudo duas vezes".

![bg right fit](./resources/roman-kata.png)

---

# Arquitetura Testavel

TDD te obriga a desacomplar os componentes

---

# Refatoração


Segurança/Confiança para alterar o código

---
# SOLID

S - Single Responsability Principle
O - Open Close Principle
L - Liskov Substitution Principle
I - Intreface Segregation Principle
D - Dependency Inversion Principle

<https://www.youtube.com/watch?v=6SfrO3D4dHM>

<!-- _footer: https://pt.wikipedia.org/wiki/SOLID -->

---

# Integração Contínua

---

![bg right fit](./resources/gitlab-logo.svg)

Gitlab CI/CD

[GitLab CI CD Tutorial for Beginners](https://www.youtube.com/watch?v=qP8kir2GUgo)

---

```yaml
#.gitlab-ci.yaml
stages:
  - build
  - test
  - deploy
build:
  stage: build
  script:
    - echo "building..."
test:
  stage: test
  script:
    - echo "testing..."
deploy:
  stage: deploy
  script:
    - echo "deploying..."
```

<!-- _footer: https://docs.gitlab.com/ee/ci/yaml/ -->

---

Cada job é executado em um container diferente.
São executados de acordo com o seu `stage`.
Se um job falhar os jobs dos próximos `stages` não são executados.
Não compartilham arquivos de forma implicita.

---

# Build & Cache

```yaml
.php: &php
  image: php:alpine
  cache: &php-cache
    policy: pull
    key:
      files:
        - composer.lock
    paths:
      - vendor/
build:
  <<: *php
  cache:
    <<: *php-cache
    policy: pull-push
  stage: build
  script:
    - curl -sS https://getcomposer.org/installer | php
    - php composer.phar install
```

---

# Test

```yaml
test:
  <<: *php
  stage: test
  script:
    - php vendor/bin/phpunit --bootstrap vendor/autoload.php test
```

---

# Deploy

O deploy vai depender do seu ambiente de produção

Pode ser:
    - conectar via SSH em uma máquina e inciar/atualizar um serviço.
    - enviar uma imagem para o hub.docker.com
    - atualizar um serviço em um cluster k8s ( aws, google, azure...)
    - publicar um pacote

---

Podemos usar grafos no lugar dos `stages`

```yaml

test:
  needs:
    - build

```

[needs](https://docs.gitlab.com/ee/ci/yaml/#needs)

<!-- _footer: https://docs.gitlab.com/ee/ci/directed_acyclic_graph/ -->

---

# GitHub Workflows

![bg right fit](resources/github-logo.svg)

<!-- _footer: https://docs.github.com/en/actions/using-workflows -->

---

# Jenkins

![bg right fit](resources/jenkins-logo.svg)

<!-- _footer: https://www.jenkins.io/ -->

---

<!-- header: Projeto -->
<!-- _class: invert center -->

# Projeto
---


![bg fit](resources/project-01-uml.svg)

---

# Estabilidade

Estabilidade é +/- Dificuldade para mudar

A estabilidade é diretamente proporcional aos motivos para não mudar e inversamente proporcional aos motivos para mudar

`S = O / ( O + I)`

- `S = 0` Estável
- `S = 1` Instável
- `I` "Import range" Quantidade de "exports"
- `O` "Output range" Quantidade de "imports"

---

![bg fit](./resources/unstable-component-uml.svg)
![bg fit](./resources/stable-component-uml.svg)

---

# Frameworks

- Facilitam o setup incial do projeto
- Proveem diversas utilidades
- Geralmente são bem testados

Porém:

- Fremeworks são relacionamentos de uma via

    Você precisa se adaptar para acomodar mudanças/atualizações, já o framework nem sabe que você existe

---

<!-- header: "" -->
<!-- _class: center invert cover -->

# FIM

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->

Ettore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

[![Github Badge](https://img.shields.io/badge/--blue?style=social&logo=github&link=https://github.com/ettoreleandrotognoli)](https://github.com/ettoreleandrotognoli)[![Gitlab Badge](https://img.shields.io/badge/--blue?style=social&logo=gitlab&link=https://gitlab.com/ettoreleandrotognoli)](https://gitlab.com/ettoreleandrotognoli)[![Linkedin Badge](https://img.shields.io/badge/--blue?style=social&logo=linkedin&link=https://www.linkedin.com/in/ettore-leandro-tognoli/)](https://www.linkedin.com/in/ettore-leandro-tognoli/)[![Discord Badge](https://img.shields.io/badge/--blue?style=social&logo=discord&link=https://discord.gg/Mr6yxp4ZE2)](https://discord.gg/Mr6yxp4ZE2)[![Telegram Badge](https://img.shields.io/badge/--blue?style=social&logo=telegram&link=https://t.me/ettoreleandrotognoli)](https://t.me/ettoreleandrotognoli)[![Instagram Badge](https://img.shields.io/badge/--blue?style=social&logo=instagram&link=https://www.instagram.com/ettoreleandrotognoli/)](https://www.instagram.com/ettoreleandrotognoli/)[![Twitter Badge](https://img.shields.io/badge/--blue?style=social&logo=twitter&link=https://twitter.com/EttoreTognoli)](https://twitter.com/EttoreTognoli)

Obrigado!
