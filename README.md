# LINGUAGEM DE PROGRAMAÇÃO II

<https://ettoreleandrotognoli.gitlab.io/unimar-programming-languages-ii>

Curso: SUP TEC ANALISE DES. SISTEMAS

Disciplina: 203226-LINGUAGEM DE PROGRAMAÇÃO II

Carga horária total: 80

## Objetivo:

Apresentar o contextos em que as linguagens de programação estão inseridas, principalmente relacionado ao desenvolvimento de software.

Aprofundar em conceitos de programação orientada a objetos e funcional, utilizando padrões de projeto e boas práticas de desenvolvimento.


## Ementa:

Ambientes de desenvolvimento: Linux, GIT, VMs, Containers.

Revisão de programação com inserção de boas práticas.

Recursos de programação orientada a objetos e funcional integrado com SOLID.

Gerenciamento de dependências e desenvolvimento de bibliotecas integrado com TDD e CI/CD.


## Conteúdo Programático:

- Ambiente
    - Linux
    - VMs
    - Containers
    - GIT
- Linguagem de Programação
    - Revisão
        - Variáveis
        - Tipagem
        - Operações
        - Funções
        - Estruturas de Controle
        - Estruturas de Repetição
        - Geradores
        - Recursividade
    - Orientação a Objetos
        - Classes
            - Atributos
            - Métodos
        - Interfaces
        - Herança
        - Tipos Genéricos
        - Meta programação / Reflexão
- Bibliotecas
    - Versionamento Semântico
    - Gerenciamento de Dependências
    - Desenvolvimento dirigido por testes
    - CI/CD


## Metodologia de Ensino:

As aulas são mistas com teoria e prática.
Inicialmente a práticas são voltadas para aprendizado das ferramentas, mas depois serão direcionadas para o desenvolvimento de uma biblioteca de software.

## Sistema de Avaliação:

Avaliação será baseada na qualidade do código, notas de aula e das mensagens dos commits entregues por cada aluno.


## Bibliografia Básica:

Código limpo: Habilidades práticas do Agile Software - Robert C. Martin

Arquitetura limpa: O guia do artesão para estrutura e design de software - Robert C. Martin
Robert C. Martin

Padrões de projetos: soluções reutilizáveis de software orientados a objetos- Erich Gamma; Richard Helm; Ralph Johnson; et al.


## Bibliografia Complementar:

Pro Git - Ben Straub and Scott Chacon

## Material de Apoio

[git cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

[bash cheat sheet](https://github.com/RehanSaeed/Bash-Cheat-Sheet)

[Clean Code](https://www.youtube.com/watch?v=7EmboKQH8lM&list=PLmmYSbUCWJ4x1GO839azG_BBw8rkh-zOj)

[SOLID](https://www.youtube.com/watch?v=6SfrO3D4dHM)